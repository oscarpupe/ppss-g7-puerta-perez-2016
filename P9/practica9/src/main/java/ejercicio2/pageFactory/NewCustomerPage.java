/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.pageFactory;

import static org.openqa.selenium.By.cssSelector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class NewCustomerPage {
    WebDriver driver;
    @FindBy(name="name") WebElement customerName;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[1]") WebElement customerMale;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[2]") WebElement customerFemale;
    @FindBy(name="dob") WebElement customerDob;
    @FindBy(name="addr") WebElement customerAddr;
    @FindBy(name="city") WebElement customerCity;
    @FindBy(name="state") WebElement customerState;
    @FindBy(name="pinno") WebElement customerPin;
    @FindBy(name="telephoneno") WebElement customerTelephone;
    @FindBy(name="emailid") WebElement customerEmail;
    @FindBy(name="password") WebElement customerPassword;
    @FindBy(name="sub") WebElement buttonsubmit;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p") WebElement AddTitle;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p") WebElement AddSuccessfully;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[4]/td[2]") WebElement numID;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[14]/td/a") WebElement buttonContinue;
    @FindBy(linkText="Manager") WebElement Manager;
    ManagerPage poManagerPage;
    
    public NewCustomerPage(WebDriver driver){
        this.driver=driver;
    }
    
    public String getAddTitle(){
        return AddTitle.getText();
    }
    
    public String getnumID(){
        return numID.getText();
    }
    
    public String getAddSuccessfully(){
        return AddSuccessfully.getText();
    }
    
    public ManagerPage clickContinue(){
        buttonContinue.click();
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);
        return poManagerPage;
    }
    
    public void submit(String name,String gender, String dob, String addr, String city, String state, String pin, String telephone, String email, String password){
        customerName.sendKeys(name);
        if(gender=="m"){
            customerMale.click();
        } else{
            if(gender=="f"){
                customerFemale.click();
            }
        }
        customerDob.sendKeys(dob);
        customerAddr.sendKeys(addr);
        customerCity.sendKeys(city);
        customerState.sendKeys(state);
        customerPin.sendKeys(pin);
        customerTelephone.sendKeys(telephone);
        customerEmail.sendKeys(email);
        customerPassword.sendKeys(password);
        buttonsubmit.click();
    }
    
    public ManagerPage managerPage(){
        Manager.click();
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);
        
        return poManagerPage;
    }
}
