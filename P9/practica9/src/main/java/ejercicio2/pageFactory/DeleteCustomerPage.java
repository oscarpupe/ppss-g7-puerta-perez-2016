/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(name="cusid") WebElement customerID;
    @FindBy(name="AccSubmit") WebElement buttonsubmit;
    @FindBy(xpath="/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p") WebElement deleteTitle;
    
    public DeleteCustomerPage(WebDriver driver){
        this.driver=driver;
    }
    
    public void delete(String id){
        customerID.sendKeys(id);
        buttonsubmit.click();
    }
    
    public String getDeleteTitle(){
        return deleteTitle.getText();
    }
}
