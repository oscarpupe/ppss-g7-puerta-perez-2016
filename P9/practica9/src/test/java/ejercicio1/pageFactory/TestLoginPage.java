/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.pageFactory;

import ejercicio1.pageFactory.LoginPage;
import ejercicio1.pageFactory.ManagerPage;
import org.junit.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class TestLoginPage {
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;
    
    @Before
    public void setup(){
        driver= new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }
    
    @Test
    public void test_Login_Correct(){
        String loginPageTitle = poLogin.getLoginTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poLogin.login("mngr34733", "AbEvydU");
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr34733"));
        driver.close();
    }
    
    @Test
    public void test_Login_Incorrect(){
        String loginPageTitle = poLogin.getLoginTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poLogin.login("h", "h");
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        alert.accept();
        Assert.assertTrue(mensaje.contains("User or Password is not valid"));
        driver.close();
    }
}
