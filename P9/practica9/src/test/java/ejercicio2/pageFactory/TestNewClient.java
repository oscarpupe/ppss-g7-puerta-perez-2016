/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.pageFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class TestNewClient {
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;
    NewCustomerPage poNewCustomerPage;
    DeleteCustomerPage poDeleteCustomerPage;
    
    
    @Before
    public void setup(){
        driver= new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }
    
    @Test
    public void test_NewCustomer_Correct(){
        String loginPageTitle = poLogin.getLoginTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poManagerPage=poLogin.login("mngr34733", "AbEvydU");
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr34733"));
        poNewCustomerPage=poManagerPage.newCustomer();
        Assert.assertTrue(poNewCustomerPage.getAddTitle().toLowerCase().contains("add new customer"));
        poNewCustomerPage.submit("prac", "m", "28/05/1994", "Calle x", "Alicante", "Alicante", "123456", "99999999", "prac@alu.ua.es", "123456");
        Assert.assertTrue(poNewCustomerPage.getAddSuccessfully().toLowerCase().contains("customer registered successfully"));
        String id= poNewCustomerPage.getnumID();
        poManagerPage=poNewCustomerPage.clickContinue();
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr34733"));
        poDeleteCustomerPage=poManagerPage.deleteCustomer();
        Assert.assertTrue(poDeleteCustomerPage.getDeleteTitle().toLowerCase().contains("delete customer form"));
        poDeleteCustomerPage.delete(id);
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        alert.accept();
        Alert alert2 = driver.switchTo().alert();
        String mensaje2 = alert.getText();
        alert.accept();
        driver.close();
    }
    
    @Test
    public void test_NewCustomer_Incorrect(){
        String loginPageTitle = poLogin.getLoginTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poManagerPage=poLogin.login("mngr34733", "AbEvydU");
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr34733"));
        poNewCustomerPage=poManagerPage.newCustomer();
        Assert.assertTrue(poNewCustomerPage.getAddTitle().toLowerCase().contains("add new customer"));
        poNewCustomerPage.submit("p", "m", "29/01/1994", "Calle x", "Alicante", "Alicante", "123456", "99999999", "p@alu.ua.es", "123456");
        Assert.assertTrue(poNewCustomerPage.getAddSuccessfully().toLowerCase().contains("customer registered successfully"));
        String id= poNewCustomerPage.getnumID();
        poManagerPage=poNewCustomerPage.clickContinue();
        poNewCustomerPage=poManagerPage.newCustomer();
        Assert.assertTrue(poNewCustomerPage.getAddTitle().toLowerCase().contains("add new customer"));
        poNewCustomerPage.submit("p", "m", "29/01/1994", "Calle x", "Alicante", "Alicante", "123456", "99999999", "p@alu.ua.es", "123456");
        Alert alert = driver.switchTo().alert();
        String mensaje1 = alert.getText();
        alert.accept();
        poManagerPage=poNewCustomerPage.managerPage();
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr34733"));
        poDeleteCustomerPage=poManagerPage.deleteCustomer();
        Assert.assertTrue(poDeleteCustomerPage.getDeleteTitle().toLowerCase().contains("delete customer form"));
        poDeleteCustomerPage.delete(id);
        Alert alert2 = driver.switchTo().alert();
        String mensaje2 = alert.getText();
        alert.accept();
        Alert alert3 = driver.switchTo().alert();
        String mensaje3 = alert.getText();
        alert.accept();
        driver.close();
    }
}
