/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio5;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import junit.framework.Assert;
import org.easymock.EasyMock;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author ppss
 */
public class ListadosTest {
    Connection conexionMock;
    ResultSet resultMock;
    Statement statementMock;
    Listados listados;
    String esperado;
    
    @Test
    public void TestC1(){
        conexionMock = EasyMock.createMock(Connection.class);
        resultMock = EasyMock.createNiceMock(ResultSet.class);
        statementMock = EasyMock.createMock(Statement.class);
        String tableName = "alumnos";
        String real = "";
        esperado = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
        
        try {
            EasyMock.expect(conexionMock.createStatement()).andReturn(statementMock);
            EasyMock.expect(statementMock.executeQuery("SELECT apellido1, apellido2, nombre FROM" + tableName)).andReturn(resultMock);
            EasyMock.expect(resultMock.next()).andReturn(Boolean.TRUE);
            EasyMock.expect(resultMock.getString("apellido1")).andReturn("Garcia");
            EasyMock.expect(resultMock.getString("apellido2")).andReturn("Molina");
            EasyMock.expect(resultMock.getString("nombre")).andReturn("Ana");
            EasyMock.expect(resultMock.next()).andReturn(Boolean.TRUE);
            EasyMock.expect(resultMock.getString("apellido1")).andReturn("Lacalle");
            EasyMock.expect(resultMock.getString("apellido2")).andReturn("Verna");
            EasyMock.expect(resultMock.getString("nombre")).andReturn("Jose Luis");
            EasyMock.expect(resultMock.next()).andReturn(Boolean.FALSE);
            EasyMock.replay(conexionMock);
            EasyMock.replay(resultMock);
            EasyMock.replay(statementMock);
            
            listados = new Listados();
            real = listados.porApellidos(conexionMock, tableName);

            EasyMock.verify(conexionMock);
            EasyMock.verify(resultMock);
            EasyMock.verify(statementMock);
            
        } catch (SQLException ex) {
        }
        Assert.assertEquals(esperado, real);
    }
    
    @Test
    public void TestC2(){
        conexionMock = EasyMock.createMock(Connection.class);
        resultMock = EasyMock.createNiceMock(ResultSet.class);
        statementMock = EasyMock.createMock(Statement.class);
        String tableName = "alumnos";
        String real = "";
        esperado = "";
        
        try {
            EasyMock.expect(conexionMock.createStatement()).andReturn(statementMock);
            EasyMock.expect(statementMock.executeQuery("SELECT apellido1, apellido2, nombre FROM" + tableName)).andReturn(resultMock);
            EasyMock.expect(resultMock.next()).andReturn(Boolean.TRUE);
            EasyMock.expectLastCall().andThrow(new SQLException());
            EasyMock.replay(conexionMock);
            EasyMock.replay(resultMock);
            EasyMock.replay(statementMock);
            
            listados = new Listados();
            real = listados.porApellidos(conexionMock, tableName);

            EasyMock.verify(conexionMock);
            EasyMock.verify(resultMock);
            EasyMock.verify(statementMock);
            
        } catch (SQLException ex) {
            
        }
        Assert.assertEquals(esperado, real);
    }
}
