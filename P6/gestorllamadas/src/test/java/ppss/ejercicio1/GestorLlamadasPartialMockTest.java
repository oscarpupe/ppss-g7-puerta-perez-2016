 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ppss
 */
public class GestorLlamadasPartialMockTest {
    private Calendario mock;
    private GestorLlamadas partial;
    
    @Test
    public void testC1(){
        mock = EasyMock.createMock(Calendario.class);
        partial = EasyMock.createMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock();
        EasyMock.expect(mock.getHoraActual()).andReturn(15);
        EasyMock.expect(partial.getCalendario()).andReturn(mock);
        EasyMock.replay(mock,partial);
        double result=partial.calculaConsumo(10);
        EasyMock.verify(mock,partial);
        Assert.assertEquals(208, result, 0.001);
    }
    
    public void testC2(){
        mock = EasyMock.createMock(Calendario.class);
        partial = EasyMock.createMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock();
        EasyMock.expect(mock.getHoraActual()).andReturn(22);
        EasyMock.expect(partial.getCalendario()).andReturn(mock);
        EasyMock.replay(mock,partial);
        double result=partial.calculaConsumo(10);
        EasyMock.verify(mock,partial);
        Assert.assertEquals(105, result, 0.001);
    }
}
