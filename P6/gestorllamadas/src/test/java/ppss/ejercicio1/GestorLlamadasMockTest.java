package ppss.ejercicio1;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ppss
 */
public class GestorLlamadasMockTest {
    private TestableGestorLlamadas gll;
    private Calendario mock;
    
    @Before
    public void inicializacion(){
        mock=EasyMock.createMock(Calendario.class);
        gll = new TestableGestorLlamadas();
        gll.setCalendario(mock);
    }
 
    @Test
    public void testC1(){
        EasyMock.expect(mock.getHoraActual()).andReturn(15);
        EasyMock.replay(mock);
        double result = gll.calculaConsumo(10);
        EasyMock.verify(mock);
        Assert.assertEquals(208, result, 0.001);
    }
    
    @Test
    public void testC2(){
        EasyMock.expect(mock.getHoraActual()).andReturn(22);
        EasyMock.replay(mock);
        double result = gll.calculaConsumo(10);
        EasyMock.verify(mock);
        Assert.assertEquals(105, result, 0.001);
    }
}
