/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import junit.framework.Assert;
import org.easymock.EasyMock;
import org.easymock.MockType;
import org.junit.Test;

/**
 *
 * @author ppss
 */
public class WebClientMockTest {
    private TestableWebClient twcl;
    private HttpURLConnection mock;
    String resultadoEsperado, resultadoReal;
    
    @Test
    public void TestC1() throws IOException{
        resultadoEsperado= "Funciona";
        mock = EasyMock.createMock(HttpURLConnection.class);
        EasyMock.expect(mock.getInputStream()).andReturn(new ByteArrayInputStream("Funciona".getBytes()));
        EasyMock.replay(mock);
        twcl = new TestableWebClient();
        twcl.setHttp(mock);
        String resultadoReal = twcl.getContent(new URL ("http://www.ua.es"));
        EasyMock.verify(mock);
        Assert.assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void TestC2() throws IOException{
        resultadoEsperado= null;
        mock = EasyMock.createMock(HttpURLConnection.class);
        EasyMock.expect(mock.getInputStream()).andThrow(new IOException());
        EasyMock.replay(mock);
        twcl = new TestableWebClient();
        twcl.setHttp(mock);
        String resultadoReal = twcl.getContent(new URL ("http://www.ua.es"));
        EasyMock.verify(mock);
        Assert.assertEquals(resultadoEsperado, resultadoReal);
    }
}
