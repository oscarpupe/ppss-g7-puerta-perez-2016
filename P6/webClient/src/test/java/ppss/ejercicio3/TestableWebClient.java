/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author ppss
 */
public class TestableWebClient extends WebClient{
    private HttpURLConnection http;
    
    @Override
    protected HttpURLConnection createHttpURLConnection(URL url) throws IOException {
        return http;
    }
    
    public void setHttp(HttpURLConnection http){
        this.http=http;
    }
}
