/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import junit.framework.Assert;
import org.easymock.EasyMock;
import org.junit.Test;

/**
 *
 * @author ppss
 */
public class PremioTest {
    Premio premioMock;
    ClienteWebService clienteMock;
    String resultadoEsperado, resultadoReal;
    
    @Test
    public void premioTest1() throws ClienteWebServiceException {
        resultadoEsperado = "Premiado con pez de goma";
        
        premioMock= EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(premioMock.generaNumero()).andReturn(0.01f);
        EasyMock.replay(premioMock);
        clienteMock = EasyMock.createMock(ClienteWebService.class);
        EasyMock.expect(clienteMock.obtenerPremio()).andReturn("pez de goma");
        EasyMock.replay(clienteMock);
        premioMock.cliente=clienteMock;
        resultadoReal = premioMock.compruebaPremio();
        EasyMock.verify(clienteMock);
        EasyMock.verify(premioMock);
        Assert.assertEquals(resultadoReal, resultadoReal);
    }  
    
    @Test
    public void premioTest2() throws ClienteWebServiceException {
        resultadoEsperado = "Premiado";
        
        premioMock= EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(premioMock.generaNumero()).andReturn(0.01f);
        EasyMock.replay(premioMock);
        clienteMock = EasyMock.createMock(ClienteWebService.class);
        EasyMock.expect(clienteMock.obtenerPremio()).andThrow(new ClienteWebServiceException());
        EasyMock.replay(clienteMock);
        premioMock.cliente=clienteMock;
        resultadoReal = premioMock.compruebaPremio();
        EasyMock.verify(clienteMock);
        EasyMock.verify(premioMock);
        Assert.assertEquals(resultadoReal, resultadoReal);
    }  
}
