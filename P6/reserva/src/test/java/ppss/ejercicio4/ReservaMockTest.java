/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.easymock.EasyMock;
import static org.junit.Assert.*;
import org.junit.Test;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class ReservaMockTest {
    IOperacionBO operacionMock;
    Reserva reservaMock;
    FactoriaBOs factoriaMock;
    String login, password,socio;
    boolean bd;
    String[] isbns;
    ReservaException esperado;
    
    @Test
    public void TestC1(){
        login = "xxxx";
        password = "xxxx";
        socio = "Luis";
        bd=true;
        isbns = new String[] {"11111"}; 
        esperado = new ReservaException("ERROR de permisos; ");
        
        reservaMock = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock();
        operacionMock = EasyMock.createMock(IOperacionBO.class);
        factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        EasyMock.expect(reservaMock.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(false);
        EasyMock.replay(reservaMock,operacionMock,factoriaMock);
        try{
            reservaMock.realizaReserva(login, password, socio, isbns);
        } catch (Exception e){
            assertEquals(esperado.getMessage(),e.getMessage());
        }  
        EasyMock.verify(reservaMock,operacionMock,factoriaMock);
    }
    
    @Test
    public void TestC2(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        bd=true;
        isbns = new String[] {"11111","22222"}; 
        esperado = null;
        
        reservaMock = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        operacionMock = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reservaMock.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(reservaMock.getFactoriaBOs()).andReturn(factoriaMock);
        EasyMock.expect(factoriaMock.getOperacionBO()).andReturn(operacionMock);
        try {
            operacionMock.operacionReserva(socio, isbns[0]);
            operacionMock.operacionReserva(socio, isbns[1]);
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }

        EasyMock.replay(reservaMock);
        EasyMock.replay(operacionMock);
        EasyMock.replay(factoriaMock);

        try {
            reservaMock.realizaReserva(login, password, socio, isbns);
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }

        EasyMock.verify(factoriaMock);
        EasyMock.verify(operacionMock);
        EasyMock.verify(reservaMock);
    }
    
    @Test
    public void TestC3(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        bd=true;
        isbns = new String[] {"33333"}; 
        esperado = new ReservaException("ISBN invalido:33333; ");
        
        reservaMock = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        operacionMock = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reservaMock.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(reservaMock.getFactoriaBOs()).andReturn(factoriaMock);
        EasyMock.expect(factoriaMock.getOperacionBO()).andReturn(operacionMock);
        try {
            operacionMock.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }

        EasyMock.replay(reservaMock);
        EasyMock.replay(operacionMock);
        EasyMock.replay(factoriaMock);

        try {
            reservaMock.realizaReserva(login, password, socio, isbns);
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), esperado.getMessage());
        }

        EasyMock.verify(factoriaMock);
        EasyMock.verify(operacionMock);
        EasyMock.verify(reservaMock);
        
    }
    
    @Test
    public void TestC4(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        bd=true;
        isbns = new String[] {"11111"}; 
        esperado = new ReservaException("SOCIO invalido; ");
        
        reservaMock = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        operacionMock = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reservaMock.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(reservaMock.getFactoriaBOs()).andReturn(factoriaMock);
        EasyMock.expect(factoriaMock.getOperacionBO()).andReturn(operacionMock);
        try {
            operacionMock.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }

        EasyMock.replay(reservaMock);
        EasyMock.replay(operacionMock);
        EasyMock.replay(factoriaMock);

        try {
            reservaMock.realizaReserva(login, password, socio, isbns);
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), esperado.getMessage());
        }

        EasyMock.verify(factoriaMock);
        EasyMock.verify(operacionMock);
        EasyMock.verify(reservaMock);
    }
    
    @Test
    public void TestC5(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        bd=true;
        isbns = new String[] {"11111"}; 
        esperado = new ReservaException("CONEXION invalida; ");
        
        reservaMock = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        operacionMock = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reservaMock.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(reservaMock.getFactoriaBOs()).andReturn(factoriaMock);
        EasyMock.expect(factoriaMock.getOperacionBO()).andReturn(operacionMock);
        try {
            operacionMock.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andThrow(new JDBCException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }

        EasyMock.replay(reservaMock);
        EasyMock.replay(operacionMock);
        EasyMock.replay(factoriaMock);

        try {
            reservaMock.realizaReserva(login, password, socio, isbns);
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), esperado.getMessage());
        }

        EasyMock.verify(factoriaMock);
        EasyMock.verify(operacionMock);
        EasyMock.verify(reservaMock);
    }
}
