/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import org.easymock.MockType;
import org.junit.Assert;
import org.junit.Test;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class ReservaStubTest {
    private Reserva reserva;
    private IOperacionBO operacion;
    private FactoriaBOs factoria;
    
    @Test
    public void testC1(){
        String login = "xxxx";
        String password = "xxxx";
        String socio = "Luis";
        String isbns[] = {"11111"};
        String actual = "";
        String esperado = "ERROR de permisos; ";
        
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock(MockType.NICE);
        EasyMock.expect(reserva.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject(Usuario.class))).andStubReturn(Boolean.FALSE);
        replay(reserva);
        
        try{
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException ex) {
            actual = ex.getMessage();
        }
        
        verify(reserva);
        Assert.assertEquals(esperado, actual);
    }
    
    @Test
    public void testC2(){
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String isbns[] = {"11111","22222"};
        String actual = "";
        String esperado = "";
        
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock(MockType.NICE);
        factoria = EasyMock.createMock(FactoriaBOs.class);
        operacion = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reserva.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject(Usuario.class))).andStubReturn(Boolean.TRUE);
        EasyMock.expect(factoria.getOperacionBO()).andStubReturn(operacion);
        EasyMock.expect(reserva.getFactoriaBOs()).andStubReturn(factoria);
        try {
            operacion.operacionReserva(socio, isbns[0]);
            operacion.operacionReserva(socio, isbns[1]);
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }
        replay(reserva);
        replay(factoria);
        replay(operacion);
        try{
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException ex) {
            actual = ex.getMessage();
        }
        verify(factoria);
        verify(operacion);
        verify(reserva);
        Assert.assertEquals(esperado, actual);
    }
    
    @Test
    public void testC3(){
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String isbns[] = {"33333",};
        String actual = "";
        String esperado = "ISBN invalido:33333; ";
        
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock(MockType.NICE);
        factoria = EasyMock.createMock(FactoriaBOs.class);
        operacion = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reserva.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject(Usuario.class))).andStubReturn(Boolean.TRUE);
        EasyMock.expect(factoria.getOperacionBO()).andStubReturn(operacion);
        EasyMock.expect(reserva.getFactoriaBOs()).andStubReturn(factoria);
        try {
            operacion.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andStubThrow(new IsbnInvalidoException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }
        replay(reserva);
        replay(factoria);
        replay(operacion);
        try{
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException ex) {
            actual = ex.getMessage();
        }
        verify(factoria);
        verify(operacion);
        verify(reserva);
        Assert.assertEquals(esperado, actual);
    }
    
    @Test
    public void testC4(){
        String login = "ppss";
        String password = "ppss";
        String socio = "Pepe";
        String isbns[] = {"11111",};
        String actual = "";
        String esperado = "SOCIO invalido; ";
        
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock(MockType.NICE);
        factoria = EasyMock.createMock(FactoriaBOs.class);
        operacion = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reserva.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject(Usuario.class))).andStubReturn(Boolean.TRUE);
        EasyMock.expect(factoria.getOperacionBO()).andStubReturn(operacion);
        EasyMock.expect(reserva.getFactoriaBOs()).andStubReturn(factoria);
        try {
            operacion.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }
        replay(reserva);
        replay(factoria);
        replay(operacion);
        try{
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException ex) {
            actual = ex.getMessage();
        }
        verify(factoria);
        verify(operacion);
        verify(reserva);
        Assert.assertEquals(esperado, actual);
    }
    
    @Test
    public void testC5(){
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String isbns[] = {"11111",};
        String actual = "";
        String esperado = "CONEXION invalida; ";
        
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs","compruebaPermisos").createMock(MockType.NICE);
        factoria = EasyMock.createMock(FactoriaBOs.class);
        operacion = EasyMock.createMock(IOperacionBO.class);
        EasyMock.expect(reserva.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject(Usuario.class))).andStubReturn(Boolean.TRUE);
        EasyMock.expect(factoria.getOperacionBO()).andStubReturn(operacion);
        EasyMock.expect(reserva.getFactoriaBOs()).andStubReturn(factoria);
        try {
            operacion.operacionReserva(socio, isbns[0]);
            EasyMock.expectLastCall().andThrow(new JDBCException());
        } catch (IsbnInvalidoException e) {
            e.printStackTrace();
        } catch (JDBCException e) {
            e.printStackTrace();
        } catch (SocioInvalidoException e) {
            e.printStackTrace();
        }
        replay(reserva);
        replay(factoria);
        replay(operacion);
        try{
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException ex) {
            actual = ex.getMessage();
        }
        verify(factoria);
        verify(operacion);
        verify(reserva);
        Assert.assertEquals(esperado, actual);
    }
}
