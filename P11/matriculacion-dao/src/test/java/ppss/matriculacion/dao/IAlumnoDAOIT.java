/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.matriculacion.dao;

import java.util.Calendar;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.apache.log4j.BasicConfigurator;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import ppss.matriculacion.to.AlumnoTO;
/**
 *
 * @author ppss
 */
public class IAlumnoDAOIT {
    private FuenteDatosJDBC _fuente;
    
    public static final String TABLE_MATRICULACION = "matriculacion";

    private IDatabaseTester databaseTester;
    
    @BeforeClass
  public static void only_once() {
//    Para evitar el mensaje   
//    Running ppss.dbunitexample.TestDBUnit
//    log4j:WARN No appenders could be found for logger (org.dbunit.assertion.SimpleAssert).
//    log4j:WARN Please initialize the log4j system properly.  
    BasicConfigurator.configure();
    
  }
  @Before
  public void setUp() throws Exception {
    databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
        		"jdbc:mysql://localhost:3306/matriculacion", "root", "ppss");
      
    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet dataSet = loader.load("/tabla2.xml");	 
      
    databaseTester.setDataSet(dataSet);

    databaseTester.onSetup();

    _fuente = FuenteDatosJDBC.getInstance();
  }
  
  @Test
  public void testA1() throws Exception{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.YEAR, 1985);
    cal.set(Calendar.MONTH, 1); // Nota: en la clase Calendar, el primer mes es 0
    cal.set(Calendar.DATE, 22);
    
    AlumnoTO alumno = AlumnoTO.class.newInstance();
    alumno.setFechaNacimiento(cal.getTime());
    alumno.setNif("33333333C");
    alumno.setNombre("Elena Aguirre Juarez");
    
    new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);

    IDatabaseConnection connection = databaseTester.getConnection();
    
    // configuramos la conexión como de tipo mysql
    // para evitar el mensaje: 
    // WARN org.dbunit.dataset.AbstractTableMetaData - Potential problem found: 
    //      The configured data type factory 'class org.dbunit.dataset.datatype.DefaultDataTypeFactory' 
    //      might cause problems with the current database 'MySQL' (e.g. some datatypes may not be supported properly). 
    //      ...
    DatabaseConfig dbconfig = connection.getConfig();
    dbconfig.setProperty("http://www.dbunit.org/properties/datatypeFactory", new MySqlDataTypeFactory());
	        
    IDataSet databaseDataSet = connection.createDataSet();
    ITable actualTable = databaseDataSet.getTable("alumnos");

    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet expectedDataSet = loader.load("/tabla3.xml");
      
    ITable expectedTable = expectedDataSet.getTable("alumnos");

    Assertion.assertEquals(expectedTable, actualTable);
   }
  
  @Test(expected=DAOException.class)
  public void testA2() throws Exception{
   
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.YEAR, 1982);
    cal.set(Calendar.MONTH, 1); // Nota: en la clase Calendar, el primer mes es 0
    cal.set(Calendar.DATE, 22);
    
    AlumnoTO alumno = AlumnoTO.class.newInstance();
    alumno.setFechaNacimiento(cal.getTime());
    alumno.setNif("11111111A");
    alumno.setNombre("Alfonso Ramirez Ruiz");
    new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);
   }
  
  @Test(expected=DAOException.class)
  public void testA3() throws Exception{
      
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.YEAR, 1982);
    cal.set(Calendar.MONTH, 1); // Nota: en la clase Calendar, el primer mes es 0
    cal.set(Calendar.DATE, 22);
    
    AlumnoTO alumno = AlumnoTO.class.newInstance();
    alumno.setFechaNacimiento(cal.getTime());
    alumno.setNif("44444444D");
    alumno.setNombre(null);
    new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);
  }

  
  @Test(expected=DAOException.class)
  public void testA4() throws Exception{
      
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.YEAR, 1982);
    cal.set(Calendar.MONTH, 1); // Nota: en la clase Calendar, el primer mes es 0
    cal.set(Calendar.DATE, 22);
    
    AlumnoTO alumno = AlumnoTO.class.newInstance();
    alumno=null;
    new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);

   }
  
  @Test(expected=DAOException.class)
  public void testA5() throws Exception{
  
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.YEAR, 1982);
    cal.set(Calendar.MONTH, 2); // Nota: en la clase Calendar, el primer mes es 0
    cal.set(Calendar.DATE, 22);
    
    AlumnoTO alumno = AlumnoTO.class.newInstance();
    alumno.setFechaNacimiento(cal.getTime());
    alumno.setNif(null);
    alumno.setNombre("Pedro Garcia Lopez");
    new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);
   }
  
  @Test
  public void testB1() throws Exception{
    
    new FactoriaDAO().getAlumnoDAO().delAlumno("11111111A");

    IDatabaseConnection connection = databaseTester.getConnection();
    
    // configuramos la conexión como de tipo mysql
    // para evitar el mensaje: 
    // WARN org.dbunit.dataset.AbstractTableMetaData - Potential problem found: 
    //      The configured data type factory 'class org.dbunit.dataset.datatype.DefaultDataTypeFactory' 
    //      might cause problems with the current database 'MySQL' (e.g. some datatypes may not be supported properly). 
    //      ...
    DatabaseConfig dbconfig = connection.getConfig();
    dbconfig.setProperty("http://www.dbunit.org/properties/datatypeFactory", new MySqlDataTypeFactory());
	        
    IDataSet databaseDataSet = connection.createDataSet();
    ITable actualTable = databaseDataSet.getTable("alumnos");

    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet expectedDataSet = loader.load("/tabla4.xml");
      
    ITable expectedTable = expectedDataSet.getTable("alumnos");

    Assertion.assertEquals(expectedTable, actualTable);
   }
  
  @Test(expected=DAOException.class)
  public void testB2() throws Exception{
      new FactoriaDAO().getAlumnoDAO().delAlumno("33333333C");
  }
}
