/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class MultipathExampleTest {
    @Test
    public void testmultiPath1(){
        MultipathExample app = new MultipathExample();
                
        int n= app.multiPath(10, 10, 10);
        Assert.assertEquals(30, n);
    }
    
    @Test
    public void testmultiPath2(){
        MultipathExample app = new MultipathExample();
                
        int n= app.multiPath(1, 1, 10);
        Assert.assertEquals(10, n);
    }
}
