/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
@Category(ConParametros.class)
@RunWith(Parameterized.class)
public class TestAlumnoConParametros {
    
    @Parameterized.Parameters(name = "Caso C{index}: validaNif({0})= {1}")
    public static Collection<Object []> data(){
        return Arrays.asList(new Object[][]{
            {"123", false}, //C1
            {"1234567AA", false}, //C2
            {"-12345678", false}, //C3
            {"00000000X", false}, //C4
            {"00000000T", true} //C5
        });
    }
    
    String nif;
    boolean esperado;
    Alumno alumno= new Alumno();
    
    public TestAlumnoConParametros(String nif,boolean esperado){
        this.nif=nif;
        this.esperado=esperado;
    }
    
    @Test
    public void testNif() {
        assertEquals(esperado, alumno.validaNif(nif));
    }
}
