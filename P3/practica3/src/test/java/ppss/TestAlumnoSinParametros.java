/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

/**
 *
 * @author ppss
 */
@Category(SinParametros.class)
public class TestAlumnoSinParametros {
    
    String nif;
    boolean resultadoEsperado, resultadoReal;
    Alumno alumno= new Alumno();
    
    @Test
    public void testC1() {
        nif="123";
        resultadoEsperado = false;
        resultadoReal = alumno.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testC2() {
        nif="1234567AA";
        resultadoEsperado = false;
        resultadoReal = alumno.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testC3() {
        nif="-12345678";
        resultadoEsperado = false;
        resultadoReal = alumno.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testC4() {
        nif="00000000X";
        resultadoEsperado = false;
        resultadoReal = alumno.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testC5() {
        nif="00000000T";
        resultadoEsperado = true;
        resultadoReal = alumno.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
}