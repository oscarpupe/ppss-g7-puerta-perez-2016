/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.Arrays;
import java.util.Collection;
import junit.framework.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
@Category(ConParametros.class)
@RunWith(Parameterized.class)
public class TestMatriculaConParametros {

    @Parameterized.Parameters(name = "Caso C{index}: calcularTasaMatricula({0},{1},{2})= {3}")
    public static Collection<Object []> data(){
        return Arrays.asList(new Object[][]{
            {20, false, true, 2000}, //C1
            {70, false, true, 250}, //C1
            {20, true, true, 250}, //C1
            {20, false, false, 500}, //C1
            {60, false, true, 400}, //C1
        });
    }
    
    private float esperado;
    private int edad;
    private boolean familiaNumerosa, repetidor;
    private Matricula matricula = new Matricula();
    
    public TestMatriculaConParametros(int edad, boolean familiaNumerosa, boolean repetidor, float esperado){
        this.edad=edad;
        this.familiaNumerosa=familiaNumerosa;
        this.repetidor=repetidor;
        this.esperado=esperado;
    }
    
    @Test
    public void testMatricula() {
        assertEquals(esperado, matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor), 0.002f);
    }
}
