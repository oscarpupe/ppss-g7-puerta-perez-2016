/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

/**
 *
 * @author ppss
 */
@Category(SinParametros.class)
public class TestMatriculaSinParametros {
    int edad;
    boolean familiaNumerosa;
    boolean repetidor;
    float resultadoEsperado, resultadoReal;
    Matricula matricula= new Matricula();
    
    @Test
    public void testC1() {
        edad=20;
        familiaNumerosa=false;
        repetidor=true;
        resultadoEsperado = 2000.00f;
        resultadoReal = matricula.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC2() {
        edad=70;
        familiaNumerosa=false;
        repetidor=true;
        resultadoEsperado = 250.00f;
        resultadoReal = matricula.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC3() {
        edad=20;
        familiaNumerosa=true;
        repetidor=true;
        resultadoEsperado = 250.00f;
        resultadoReal = matricula.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC4() {
        edad=20;
        familiaNumerosa=false;
        repetidor=false;
        resultadoEsperado = 500.00f;
        resultadoReal = matricula.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC5() {
        edad=60;
        familiaNumerosa=false;
        repetidor=true;
        resultadoEsperado = 400.00f;
        resultadoReal = matricula.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
}