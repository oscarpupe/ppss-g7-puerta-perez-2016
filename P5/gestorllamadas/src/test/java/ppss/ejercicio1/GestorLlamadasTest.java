/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    int minutos;
    double resultadoEsperado,resultadoReal;
    TestableGestorLlamadas testable= new TestableGestorLlamadas();
    
    @Test
    public void testC1(){
        minutos=10;
        testable.setHora(15);
        resultadoEsperado=208;
        resultadoReal= testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC2(){
        minutos=10;
        testable.setHora(22);
        resultadoEsperado=105;
        resultadoReal= testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
}
