/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    int minutos;
    double resultadoEsperado,resultadoReal;
    TestableGestorLlamadas testable= new TestableGestorLlamadas();
    CalendarioStub calendario= new CalendarioStub();
    
    @Test
    public void testC1(){
        minutos=10;
        resultadoEsperado=208;
        calendario.setHoraActual(15);
        testable.setCalendario(calendario);
        resultadoReal= testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
    @Test
    public void testC2(){
        minutos=10;
        resultadoEsperado=208;
        calendario.setHoraActual(15);
        testable.setCalendario(calendario);
        resultadoReal= testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }
    
}
