/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

/**
 *
 * @author ppss
 */
public class IOperacionFactory {
    private static IOperacionBO operacion=null;
    
    public static IOperacionBO Create(){
        if(operacion != null){
            return operacion;
        } else {
            return new Operacion();
        }
    }
    
    public static void setOperacion(IOperacionBO operacion){
        operacion=operacion;
    }
}
