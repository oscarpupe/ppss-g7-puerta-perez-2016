/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.ejercicio3.excepciones.ReservaException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class ReservaTest {
    @Parameterized.Parameters(name = "Test {index}: {0},{1},{2},{3},{4} = {5}")
    public static Collection<Object[]>data(){
        return Arrays.asList(new Object[][]{
                {"xxxx","xxxx","Luis",true,new String[]{"11111"},new ReservaException("ERROR de permisos; ")},
                {"ppss","ppss","Luis",true,new String[]{"11111","22222"},null},
                {"ppss","ppss","Luis",true,new String[]{"33333"},new ReservaException("ISBN invalido:33333; ")},
                {"ppss","ppss","Pepe",true,new String[]{"11111"},new ReservaException("SOCIO invalido; ")},
                {"ppss","ppss","Luis",true,new String[]{"11111"},new ReservaException("CONEXION invalida; ")},
        });
    }

    Reserva reserva = null;
    String login,pass,socio;
    boolean bd;
    String[] isbns;
    Exception esperado = null;

    public ReservaTest(String login, String pass, String socio, boolean bd, String[] isbns, Exception e){
        this.login = login;
        this.pass = pass;
        this.socio = socio;
        this.bd = bd;
        this.isbns = isbns;
        esperado = e;
    }

    @Test
    public void testReserva() {

        reserva = new TestableReserva();

        OperacionStub stub = new OperacionStub();
        stub.setAccesoBBDD(bd);
        IOperacionFactory op = new IOperacionFactory();
        op.setOperacion(stub);

        try{
            reserva.realizaReserva(login,pass,socio,isbns);
        }catch(ReservaException e){
            if(esperado!=null)
                assertEquals(esperado.getMessage(),e.getMessage());
            else
                fail("Se ha lanzado la excepción "+e.toString()+" y no debería haberse lanzado");
        }catch(Exception e){
            fail("Excepcion no esperada" + e.toString());
        }
    }
}