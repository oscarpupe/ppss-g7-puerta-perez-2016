/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.Arrays;
import java.util.Collection;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.ejercicio3.excepciones.ReservaException;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class ReservaIntegracionTest {
    @Parameterized.Parameters(name = "Test {index}: {0},{1},{2},{3},{4} = {5}")
    public static Collection<Object[]>data(){
        return Arrays.asList(new Object[][]{
                {"xxxx","xxxx","Luis",true,new String[]{"11111"},new ReservaException("ERROR de permisos; ")},
                {"ppss","ppss","Luis",true,new String[]{"11111","22222"},null},
                {"ppss","ppss","Luis",true,new String[]{"33333"},new ReservaException("ISBN invalido:33333; ")},
                {"ppss","ppss","Pepe",true,new String[]{"11111"},new ReservaException("SOCIO invalido; ")},
                {"ppss","ppss","Luis",true,new String[]{"11111"},new ReservaException("CONEXION invalida; ")},
        });
    }
    
    Reserva reserva = null;
    String login,pass,socio;
    boolean bd;
    String[] isbns;
    Exception esperado = null;
    
    public ReservaIntegracionTest(String login, String pass, String socio, boolean bd, String[] isbns, Exception e){
        this.login = login;
        this.pass = pass;
        this.socio = socio;
        this.bd = bd;
        this.isbns = isbns;
        esperado = e;
    }
    
    @Test
    public void testReserva() {
        String real = null;
        try{
            reserva.realizaReserva(login, pass, socio, isbns);
        } catch (Exception e){
            real = e.getMessage();
        }
        Assert.assertEquals(esperado.getMessage(), real);
    }
}
