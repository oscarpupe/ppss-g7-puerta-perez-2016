/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.ArrayList;
import java.util.Arrays;
import ppss.ejercicio3.excepciones.IsbnInvalidoException;
import ppss.ejercicio3.excepciones.JDBCException;
import ppss.ejercicio3.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class OperacionStub implements IOperacionBO{
    private boolean bd = false;

    @Override
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {

        ArrayList<String> isbns = new ArrayList<String>(Arrays.asList(new String[] {"11111","22222"}));

        if(!bd)
            throw new JDBCException();

        if(!isbns.contains(isbn))
            throw new IsbnInvalidoException();

        if(!socio.equals("Luis"))
            throw new SocioInvalidoException();

    }
    public void setAccesoBBDD(boolean b){
        bd = b;
    }
}